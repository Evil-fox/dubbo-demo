package com.dubbo.service;

import dubbo.pojo.User;

public interface UserService {

	public User selectByUserNum(String userNum);
	
	public User selectByUser(User user);

}
