package com.mg.dubbo.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dubbo.service.OrderService;
import com.dubbo.service.UserService;

import dubbo.pojo.Order;
import dubbo.pojo.User;

@Controller
public class AController {
	@Autowired
	private UserService userService;
	@Autowired
	private OrderService orderService;
	
	
	@RequestMapping(value="/user")
	@ResponseBody
	public User getUserdata(){
		User user = userService.selectByUserNum("110");
		return user;
	}
	
	@RequestMapping(value="/entity")
	@ResponseBody
	public User getList(User user){
		return userService.selectByUser(user);
	}
	
	@RequestMapping(value="/order")
	@ResponseBody
	public Order getdata(Order order){
		Order result = orderService.getOrder(order);
		return result;
	}
}
