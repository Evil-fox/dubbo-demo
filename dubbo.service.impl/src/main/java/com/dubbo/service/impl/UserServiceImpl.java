package com.dubbo.service.impl;

import com.dubbo.service.UserService;

import dubbo.pojo.User;


public class UserServiceImpl implements UserService{
	
	@Override
	public User selectByUserNum(String userNum) {
		User user = new User();
		user.setId(0);
		user.setAge(18);
		user.setName("奥巴马");
		user.setUserNum(userNum);
		user.setAddress("阿富汗");
		return user;
	}

	@Override
	public User selectByUser(User user) {
		user.setUserNum("fox");
		return user;
	}


}
